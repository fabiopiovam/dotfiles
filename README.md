# dotfiles

Algumas configurações GNU Linux.


## Contexto

```sh

           _,met$$$$$gg.          
        ,g$$$$$$$$$$$$$$$P.       
      ,g$$P"     """Y$$.".        
     ,$$P'              `$$$.     fabiopiovam@gmail.com
    ',$$P       ,ggs.     `$$b:   ---------------------
    `d$$'     ,$P"'   .    $$$    OS: Debian GNU/Linux 9.12 (stretch) x86_64
     $$P      d$'     ,    $$P    Shell: zsh 5.3.1
     $$:      $$.   -    ,d$$'    
     $$;      Y$b._   _,d$P'      DE: KDE
     Y$$.    `.`"Y$$$$P"'         WM: KWin
     `$$b      "-.__              Theme: Breeze
      `Y$$                        Icons: Breeze-dark                           
       `Y$$.                      
         `$$b.                    
           `Y$$b.                 
              `"Y$b._             
                  `"""            

```


## Conteúdo

* core - tmux & vim
* manager - server admin tools
* shell - zsh configs


## Requerimentos

* [GNU Stow](https://www.gnu.org/software/stow/) $ `sudo apt install stow`
* [Git](https://git-scm.com/) $ `sudo apt install git-core`


## Download

```sh
git clone --recurse-submodules https://gitlab.com/fabiopiovam/dotfiles.git
cd dotfiles
```

## Stow (organizar symlinks)

### full

```sh
find . -maxdepth 1 -iregex "\.\/[^\.].*" -type d -exec sh -c 'stow -v -t ~/ "${0#./}" ' {} \;
```

### by module

```sh
stow -v -t ~/ core manager ...
```

## Configurar fontes (apt/source.list)

```sh
find -L ~/.source-list-d/ -iregex ".*\.list" -exec sudo cp {} /etc/apt/sources.list.d/ \;
rm -rf ~/.source-list-d
```

## Instalar pacotes

```sh
find -L ~/.apt-install/ -iregex ".*\.sh" -exec chmod +x {} \; -exec {} \;
rm -rf ~/.apt-install
```

