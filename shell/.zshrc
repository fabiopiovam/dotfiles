# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:/snap/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# export BROWSER="brave-browser"
export BROWSER="firefox"
export TERMINAL="zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

# ZSH_THEME="miloshadzic"
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
ZSH_THEME_RANDOM_CANDIDATES=(
  # "robbyrussell"
  "sorin"
  # "theunraveler"
  "miloshadzic"
  # "sonicradish"
)

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=./my-oh-my-zsh
ZSH_CUSTOM=$HOME/.oh-my-zsh.custom

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  zsh-autosuggestions
  python
  # django
  # vi-mode
  colorize
  colored-man-pages
  lol
  man
  extract
  tmux
  heroku
  docker
  docker-compose
)

source $ZSH/oh-my-zsh.sh
source $ZSH_CUSTOM/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=pt_BR.UTF-8
export LC_CTYPE=pt_BR.UTF-8
export LC_ALL=pt_BR.UTF-8

# User configuration
# Always work in a tmux session if tmux is installed
# https://github.com/chrishunt/dot-files/blob/master/.zshrc
# if which tmux 2>&1 >/dev/null; then
#   if [ $TERM != "screen-256color" ] && [  $TERM != "screen" ]; then
#     # tmux attach -t hack || tmux new -s hack; exit
#     # tmux
#     if [ "$TMUX" = "" ]; then tmux; fi
#   fi
# fi

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi
export EDITOR='vim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
alias zshconfig="vim ~/.zshrc"

alias ohmyzsh="vim ~/.oh-my-zsh"
alias dotfiles="cd ~/.dotfiles"

# alias cat_original="cat"
# alias cat="pygmentize -g"
alias pipfreeze="pip freeze | grep -v 'pkg-resources'"
alias loro="curl parrot.live"
alias lsK='du -h | egrep -v "\./.+/" | sort -h'

# todos
alias todos='vim ~/.todos.md'

# python aliases
alias venv='source venv/bin/activate'
alias run='python manage.py runserver $1'
alias migrate='python manage.py migrate'
alias makemigrations='python manage.py makemigrations'
alias testes='python manage.py test '

function set_ytabname() {
    qdbus org.kde.yakuake /yakuake/tabs org.kde.yakuake.setTabTitle $(qdbus org.kde.yakuake /yakuake/sessions activeSessionId) $1
}
# tmux aliases
function tmux_session() {
    session_name="$1"
    set_ytabname $session_name
    if tmux has-session -t $session_name; then
        tmux attach-session -t $session_name
    else
        tmux new -s $session_name
    fi
}
alias ytabname="set_ytabname $1"
alias t='tmux_session'
alias tmedia='t media'
alias tsocial='t social'
alias tdotfiles='t dotfiles'

alias gitdiff="git diff"
alias gdiff="git diff"
alias gcommit="git commit -m $1"

if [ -f ~/.zshrc.private ]; then
    . ~/.zshrc.private
fi

