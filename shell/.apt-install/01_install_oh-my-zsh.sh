#!/bin/bash

sudo apt install -y zsh zsh-syntax-highlighting

cd ~/
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

chsh -s $(which zsh)
