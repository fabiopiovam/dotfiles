" HowTo config:
" * pesquisar case insensitive
" * tamanho de indentacao por formato de arquivo
" * com a tela dividida, redimencionar workspace

" execução do gerenciador de plugins
execute pathogen#infect()

" colorir o editor
syntax on

" esquema de cores escuro
colorscheme transparent

" Stop acting like classic vi
set nocompatible            " disable vi compatibility mode
set history=1000            " increase history size
set noswapfile              " don't create swapfiles
set nobackup                " don't backup, use git!

" tamanho da indentacao
set tabstop=4
" identifica o tipo de arquivo e indenta
filetype plugin indent on
" set autoindent
" coerencia entre indentacao e TAB
set shiftwidth=4
" comportamento usual do backspace
" set backspace=4
set backspace=indent,eol,start
" usar espacos no lugar de tab
set expandtab
" backspace respeitar indentacao
set softtabstop=4
set hidden


let g:closetag_filenames = "*.html,*.xhtml,*.phtml,*.html.erb,*.xml.erb,*.xml"


" busca incremental - feedback enquanto busco
set incsearch
" destaque nos resultados
set hlsearch


" remapear atalho do Emmet
let g:user_emmet_leader_key=','


" salvar na codificacao desejada
set fileencoding=utf-8
" visualizar na codificacao desejada
set encoding=utf-8

" NERDTree config

" The working directory is always the one
" where the active buffer is located.
set autochdir

" I make sure the working directory is set correctly.
let NERDTreeChDirMode=2
" I hit <leader>n to open NERDTree.
nnoremap <leader>n :NERDTree<CR>
" map <Leader>n <plug>NERDTreeTabsToggle<CR>
" map <Leader>nt :NERDTreeToggle<CR>
" let NERDTreeQuitOnOpen=1
" let NERDTreeWinSize=20
let NERDTreeShowHidden=1

" Transparencia
hi Normal guibg=NONE ctermbg=NONE

" set showmode            " always show which more are we in
" set laststatus=2        " always show statusbar
" set wildmenu            " enable visual wildmenu

set nowrap              " don't wrap long lines
set number              " show line numbers
set relativenumber      " show numbers as relative by default
" set showmatch           " higlight matching parentheses and brackets

" Disable Folding
let g:vim_markdown_folding_disabled = 1

" !important to security
" source https://thehackernews.com/2019/06/linux-vim-vulnerability.html
set modelines=0
set nomodeline

