#!/bin/bash

# aptitude is the recommended package manager for Debian GNU/Linux systems, and is described in aptitude, Section 8.1.3.
# https://www.debian.org/doc/manuals/debian-faq/ch-uptodate.en.html
sudo apt install -y aptitude

sudo apt install -y vim curl tmux pipsi ranger rsync

