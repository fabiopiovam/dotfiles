#!/bin/bash

# Pós-instalação
# TODO: Mini readme no cabeçalho do script

# Configurar comportamento da sessão [hibernação; descanso de tela, ...];
# TODO: Verificar dotfiles referentes a esse logind

# Atualizar ;
apt update

# Instalar aptitude git vim;
apt install -y aptitude git vim stow

# Configurar `visudo`
# TODO: Tentar com sudoer

# Wallpaper
# Download repos
# TODO: clonar fork wallpaper
# TODO: Programa para gerenciar e configurar dotfiles

# Configuração de monitor estendido
# TODO: pensar como organizar

# Download .dotfiles
cd ~/
git clone --recurse-submodules https://gitlab.com/fabiopiovam/dotfiles.git
mv dotfiles .dotfiles

# Instalar dotfiles
# TODO: Criar .dotfiles/install.sh
# Utilizar stow!

# Configurar APT SourceList;
# TODO: versionar em dotfiles

# Definição de chave para comunicação ssh
# TODO: Criar metodo de definição
# * uso atual: gitlab; github; gcloud/DO Machine; ssh; scp
# * Começar usar com: rsync hd com.; streaming; 

# definições cron
# TODO: Separar diretório cron/[app/task/slug-tags/...].conf

# python3.6 - youtube-dl using..

